#include "Serial_Def.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>

/* Serial Port device */
char* port = 0;

/* TTY device */
int ttydev = 0;


uint8_t bitmap[16] = {
		0b00000000,
		0b00000000,
		0b00000000,
		0b00000000,
		0b00000000,
		0b00000000,
		0b00000000,
		0b00000000,
		0b10000001,
		0b11111111,
		0b10000001,
		0b00000000,
		0b11111111,
		0b00111000,
		0b11111111,
		0b00000000
};



static void usage( void ){
	printf("hsc_disp -p <serial_device>\n");
	printf("\t-p:\tSerial port device descriptor\n");
}

int main( int argc, char** argv ){
	if( argc < 2 ){
		printf("Incorrect number of arguments\n");
		usage();
		return -1;
	}

	/* Get and parse options */
	int opt = 0;
	while( (opt = getopt( argc, argv, "p:")) != -1 ){
		switch( opt ){
			case 'p':
				/* Get serial port device */
				port = malloc( strlen( optarg ) );
				strcpy( port, optarg );
				break;
			default:
				printf("Unknown option: %s\n", optarg);
				usage();
				return -1;
		}
	}

	printf("Port: %s\n", port);

	/* Open serial port */
	ttydev = open( port, O_RDWR | O_NOCTTY | O_SYNC );

	if( ttydev == 0 ){
		printf("Could not open serial device %s\n", port);
		close( ttydev );
		return -1;
	}

	/* Configure serial port */
//	if( config_tty( ttydev, 115200, PARITY_NONE ) == -1 ){
//		printf("Could not configure TTY\n");
//		close( ttydev );
//		return -1;
//	}

	serial_write( ttydev, bitmap, 15 );
	tcflush( ttydev, TCIOFLUSH );


}
