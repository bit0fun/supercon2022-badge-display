#include "Serial_Def.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/select.h>



/* Setup TTY settings */
int config_tty( int fd, unsigned int baud, unsigned int parity ){
	struct termios tty;
	memset( &tty, 0, sizeof( struct termios ) );
	if( tcgetattr( fd, &tty) != 0){
		printf( "Error inside tcgetattr: %d\n", errno);
		return -1;
	}

	/* 8 bit data field, no parity */
	tty.c_cflag = ( tty.c_cflag & ~CSIZE ) | CS8;
	if( parity == PARITY_NONE)
		tty.c_cflag = ~(PARENB | PARODD );
	else if( parity == PARITY_EVEN) 
		tty.c_cflag = PARENB | ~( PARODD );
	else if( parity == PARITY_ODD )
		tty.c_cflag = PARENB | PARODD;
	else	/* Default to no parity */
		tty.c_cflag = ~(PARENB | PARODD );

	tty.c_cflag &= ~(CSTOPB | CRTSCTS );

	tty.c_iflag &= ~(IGNBRK | IXON | IXOFF | IXANY );		/* Disable break processing, xon\xoff */
	tty.c_lflag &= 0;//~(ICANON) ; 			/* No software flow control, non canonical mode */
	tty.c_oflag = 0; 			/* No remapping or delays */
	tty.c_cc[VMIN] = 0xff;			/* Don't block on reads */
	tty.c_cc[VTIME] = 100;		/* Timeout after 500ms*/

	if( cfsetospeed( &tty, baud ) == -1 ){
		printf("Could not set baud rate\n");
		return -1;
	}
	if( cfsetispeed( &tty, baud ) == -1 ){
		printf("Could not set baud rate\n");
		return -1;
	}


	if( tcsetattr(fd, TCSANOW, &tty ) != 0 ){
		printf( "Error inside tcsetattr: %d\n", errno);		
		return -1;
	}

	return 0;
}

int serial_write( int fd, uint8_t *data, unsigned int length ){
	write( fd, data, length );
	usleep(10000);
	tcflush( fd, TCOFLUSH );
	return 0;
}

int serial_read( int fd, uint8_t *data, unsigned int length ){
	fd_set set;
	struct timeval timeout;
	int returnval = 0;

	FD_ZERO(&set);
	FD_SET(fd, &set);

	timeout.tv_sec = 0;
	timeout.tv_usec = 500000; 

	/* Do timeout check for bytes */
	returnval = select( fd + 1, &set, NULL, NULL, &timeout);
	if( returnval == -1 ){
		printf("Internal Error reading!\n");
		return -1;
	}
	else if(returnval == 0){
		return 0;
	}
	else{
		return read( fd, data, length);
	}




	

}
