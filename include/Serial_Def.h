#ifndef OTA_SERIAL_DEF_H
#define OTA_SERIAL_DEF_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#define PARITY_NONE	0
#define PARITY_EVEN	1
#define PARITY_ODD  2


/* Setup TTY settings */
int config_tty( int fd, unsigned int baud, unsigned int parity );

int serial_write( int fd, uint8_t *data, unsigned int length );
int serial_read( int fd, uint8_t *data, unsigned int length );

#endif /* OTA_SERIAL_DEF_H */
